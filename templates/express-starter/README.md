# express-starter

## Pre-requisites
- npm

### Install the dependencies

Go to the project directory and run the following command to install the dependencies
```bash
npm install
```

### Run the API server for development purpose

Go to the project directory and run the following command
```bash
npm run dev
```

### Run the test with jest

Go to the project directory and run the following command
```bash
npm run test
```

### Craete the production build

Go to the project directory and run the following command
```bash
npm run build
```

