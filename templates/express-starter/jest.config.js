/*
 * @Author: joe
 * @Date: 2024-04-10 15:13:14
 * @LastEditTime: 2024-04-11 15:09:05
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\jest.config.js
 */
/** @type {import('ts-jest').JestConfigWithTsJest} */

const { pathsToModuleNameMapper } = require('ts-jest')
const { compilerOptions } = require('./tsconfig')

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['./src'],
  setupFiles: ["./src/test/test-config.ts"],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  modulePaths: [compilerOptions.baseUrl], // <-- This will be set to 'baseUrl' value
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths /*, { prefix: '<rootDir>/' } */),
};