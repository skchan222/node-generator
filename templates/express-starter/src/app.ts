/*
 * @Author: joe
 * @Date: 2024-03-12 15:35:03
 * @LastEditTime: 2024-04-12 11:41:57
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\app.ts
 */
import 'reflect-metadata';
import mongoose from 'mongoose';
import express from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import hpp from 'hpp';
import compression from 'compression';
import { useExpressServer, getMetadataArgsStorage } from 'routing-controllers';
import { defaultMetadataStorage } from 'class-transformer/cjs/storage';
import { validationMetadatasToSchemas } from 'class-validator-jsonschema';
import swaggerUi from 'swagger-ui-express';
import { routingControllersToSpec } from 'routing-controllers-openapi';
import { rateLimit } from 'express-rate-limit'
import { HttpErrorHandler } from '@/middlewares/http-error.middleware';
import { ErrorHandler } from '@/middlewares/error.middleware';
import { logger, stream } from '@/utils/logger';
import {
  NODE_ENV,
  CREDENTIALS,
  ORIGIN,
  LOG_FORMAT, 
  PORT, 
  CONNECTION_STR, 
  SWAGGER,
  RATE_CONTROL,
  RATE_LIMIT_INTERVAL,
  RATE_LIMIT_REQUEST
} from '@/config';

export class App {
  public app: express.Application;
  public port: string | number;
  public env: string;

  constructor(Controllers: Function[]) {
    this.app = express();
    this.port = parseInt(PORT as string, 10) || 3000;
    this.env = NODE_ENV || 'development';
    this.initializeMiddlewares();
    if (RATE_CONTROL.toLowerCase() === 'true')
      this.initialRateLimit();
    if (SWAGGER.toLowerCase() === 'true')
      this.initializeSwagger(Controllers);
    this.initializeRoutes(Controllers);
    this.initializeDB();
    this.initializeErrorHandler();
  }

  public getApp() {
    return this.app;
  }

  private initializeMiddlewares() {
    this.app.use(morgan(LOG_FORMAT, { stream }));
    this.app.use(hpp());
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
  }

  private initializeErrorHandler() {
    this.app.use(ErrorHandler);
  }

  private initializeRoutes(controllers: Function[]) {
    useExpressServer(this.app, {
      cors: {
        origin: ORIGIN,
        credentials: CREDENTIALS,
      },
      controllers: controllers,
      middlewares: [
        HttpErrorHandler
      ],
      defaultErrorHandler: false,
    });
  }

  public listen() {
    this.app.listen(this.port, () => {
      logger.info('================================================================');
      logger.info('listening on port ' + this.port);
      logger.info('environment: ' + this.env);
      logger.info('================================================================');
    });
  }

  private initializeSwagger(controllers: Function[]) {
    const schemas: any = validationMetadatasToSchemas({
      classTransformerMetadataStorage: defaultMetadataStorage,
      refPointerPrefix: '#/components/schemas/',
    });

    const routingControllersOptions = {
      controllers: controllers,
    };

    const storage = getMetadataArgsStorage();
    const spec = routingControllersToSpec(storage, routingControllersOptions, {
      components: {
        schemas,
        securitySchemes: {
          bearerAuth: {
            bearerFormat: 'JWT',
            scheme: 'bearer',
            type: 'http',
          },
        },
      },
      info: {
        description: 'Generated with `routing-controllers-openapi`',
        title: 'A sample API',
        version: '1.0.0',
      },
    });

    const swaggerSetup = swaggerUi.setup(spec);
    this.app.get('/api-docs/index.html', swaggerSetup);
    this.app.use('/api-docs', swaggerUi.serve);
    this.app.get('/api-docs', swaggerSetup);

  }

  private async initializeDB() {
    await mongoose.connect(CONNECTION_STR)
      .catch((error) => {
        logger.error('Cannot establish connection to MongoDB : ' + error.message);
        process.exit(1);
      });
  }

  private initialRateLimit() {
    const limiter = rateLimit({
      windowMs: parseInt(RATE_LIMIT_INTERVAL as string, 10) * 60 * 1000,
      limit: parseInt(RATE_LIMIT_REQUEST as string, 10),
      message: 'To many request from this IP address, please try again later!'
    })
    this.app.use(limiter);
  }
}