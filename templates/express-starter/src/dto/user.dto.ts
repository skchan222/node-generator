/*
 * @Author: joe
 * @Date: 2024-03-14 14:44:59
 * @LastEditTime: 2024-03-14 16:43:39
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\dto\user.dto.ts
 */
import { IsEmail, IsString, IsNotEmpty, MinLength, MaxLength } from 'class-validator'

export class NewUserDto {
    @IsEmail()
    public email: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(4, {
        message: 'username is too short',
    })
    @MaxLength(20, {
        message: 'username is too long',
    })
    public username: string;

    @MinLength(8, {
        message: 'password is too short',
    })
    @MaxLength(20, {
        message: 'password is too long',
    })
    public password: string;
}

export class loginDto {
    @IsNotEmpty()
    public username: string;

    @IsNotEmpty()
    public password: string;
}