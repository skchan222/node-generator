import { model, Schema, Document } from 'mongoose';
import { User } from '@/interfaces/user.interface';

const UserSchema = new Schema(
    {
        username: {
            type: String,
            required: [true, 'Please enter a anme'],
            index: true,
        },
        password: {
            type: String,
            required: [true, 'Please enter a password'],
        },
        email: {
            type: String,
            index: true,
        },
    },
    { timestamps: true },
)

export const UserModel = model<User & Document>('User', UserSchema);