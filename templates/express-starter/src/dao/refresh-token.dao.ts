import { model, Schema, Document, Model } from 'mongoose';
import { RefreshToken } from '@/interfaces/refresh-token.interface';
import { REFRESH_INTERVAL } from '@/config';
import { v4 as uuidv4 } from 'uuid';

interface IRefreshToken extends Model<RefreshToken> {
    generateToken(user: any): string;
    verifyExpiration(refreshToken: RefreshToken): boolean;
}

const refreshTokenSchema = new Schema(
    {
        userID: {
            type: Schema.Types.ObjectId,
            ref: "User"
        },
        username: String,
        token: String,
        expires: Date
    }
);

refreshTokenSchema.statics.generateToken = async (user: any): Promise<String> => {
    let _expiryDate = new Date(
        (new Date()).getTime() + parseInt(REFRESH_INTERVAL, 10) * 60 * 60 * 1000    // convert hour to milliseconds unit
    );

    let _refreshToken = uuidv4().toString();

    let _dataObj = new RefreshTokenModel({
        userID: user._id,
        token: _refreshToken,
        expires: _expiryDate
    });

    let refreshToken = await _dataObj.save();
    return refreshToken.token;
}

refreshTokenSchema.statics.verifyExpiration = (refreshToken: RefreshToken) => {
    return refreshToken.expires.getTime() > new Date().getTime();
}

export const RefreshTokenModel = model<RefreshToken & Document, IRefreshToken>('RefreshToken', refreshTokenSchema);
