/*
 * @Author: joe
 * @Date: 2024-03-14 15:11:39
 * @LastEditTime: 2024-04-12 11:31:00
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\services\user.service.ts
 */
import { Service } from 'typedi';
import hash from "bcrypt";
import { User } from '@/interfaces/user.interface';
import { UserModel } from '@/dao/user.dao';
import { HttpException } from '@/exceptions/http.exceptions';

@Service()
export class UserService {
    public async getAllUsers(): Promise<User[]> {
        const users: User[] = await UserModel.find();
        return users;
    }

    public async getUserById(id: string): Promise<User> {
        const user: User = await UserModel.findById(id);
        return user;
    }

    public async createUser(user: User): Promise<User> {
        const result: User[] = await UserModel.find({ username: user.username });
        if (result) throw new HttpException(500, 'User already exists');

        try {
            const hash_password = await hash(user.password, 10);
            const new_user = new UserModel({ username: user.username, password: hash_password, email: user.email })
            await new_user.save();
        } catch (err) {
            throw new HttpException(500, 'Failed to create user, error: ' + err.message);
        }
        return user;
    }

    public async updateUser(id: string, user: User): Promise<User> {
        const updateOptions = { new: true };
        const result: User[] = await UserModel.find({ username: user.username });
        if (!result) throw new HttpException(500, 'User does not already exists');

        try {
            const hash_password = await hash(user.password, 10);
            const update_user: User = { username: user.username, password: hash_password, email: user.email }
            await UserModel.findByIdAndUpdate(
                id, update_user, updateOptions
            )
        } catch (err) {
            throw new HttpException(500, 'Failed to create user, error: ' + err.message);
        }
        return user;
    }

    public async deleteUser(id: string): Promise<{result: string, error?: unknown}> {
        try {
            const result = await UserModel.findByIdAndDelete(id)
            return { result: 'succ' }
        } catch (err) {
            return { result: 'failed to delete user'}
        }
    }
}
