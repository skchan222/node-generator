/*
 * @Author: joe
 * @Date: 2024-04-25 16:22:39
 * @LastEditTime: 2024-04-26 11:46:01
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\services\request.service.ts
 */
import { Service } from 'typedi';
import request from '@/utils/request';

@Service()
export class RequestService {
    public getDetails() {
        return request({
            // url: 'http://localhost:3000/api/user/65e91c584656d714aba580fa',
            url: 'http://localhost:3000/api/user/555',
            method: 'get',
            json: false,
        }).then((res) => {
            return Promise.resolve(res)
        });
    }
}