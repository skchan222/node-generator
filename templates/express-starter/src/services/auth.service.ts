/*
 * @Author: joe
 * @Date: 2024-03-15 11:20:28
 * @LastEditTime: 2024-04-26 14:37:42
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\services\auth.service.ts
 */
import { Service } from 'typedi';
import { compare, hash } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { User } from '@/interfaces/user.interface';
import { NewUserDto, loginDto } from '@/dto/user.dto';
import { UserModel } from '@/dao/user.dao';
import { RefreshTokenModel } from '@/dao/refresh-token.dao';
import { RefreshToken } from '@/interfaces/refresh-token.interface';
import { UserToken, TokenData } from '@/interfaces/auth.interface';
import { EXPIRE_INTERVAL, SECRET_KEY } from '@/config';
import { HttpException } from '@/exceptions/http.exceptions';

@Service()
export class AuthService {
    private generateToken(username: string): UserToken {
        const tokenData: TokenData = { username: username };
        const toeknExpires = EXPIRE_INTERVAL;

        return { token: sign(tokenData, SECRET_KEY, { expiresIn: toeknExpires }) };
    }

    public async signup(user: NewUserDto): Promise<User> {
        const userInDB = await UserModel.findOne({ $or: [{ username: user.username }, { email: user.email }] })
        if (userInDB) throw new HttpException(500, 'User/Email already exists');

        try {
            const hash_password = await hash(user.password, 10);
            const new_user = new UserModel({ username: user.username, password: hash_password, email: user.email })
            await new_user.save();
        } catch (err) {
            throw new HttpException(500, 'Failed to create user, error: ' + err.message);
        }
        return user;
    }

    public async login(userCredentials: loginDto): Promise<UserToken> {
        const userInDB: User = await UserModel.findOne({ username: userCredentials.username });
        if (!userInDB) throw new HttpException(401, 'invalid username or password');

        const isAuthenticated = await compare(userCredentials.password, userInDB.password);
        if (!isAuthenticated) throw new HttpException(401, 'invalid username or password');

        const refreshToken: string = await RefreshTokenModel.generateToken(userInDB);

        let userToken = this.generateToken(userInDB.username);
        userToken.refreshToken = refreshToken;

        return userToken;
    }

    public async refreshToken(token: string): Promise<UserToken> {
        let _dbResult: RefreshToken = await RefreshTokenModel.findOne({ token: token });

        if (!_dbResult) throw new HttpException(401, 'invalid token');

        if (!RefreshTokenModel.verifyExpiration(_dbResult)) {
            // remove the record from the database
            await RefreshTokenModel.findOneAndDelete({ token: token }).exec();

            throw new HttpException(401, 'Refresh token expired');
        }

        let tokens = this.generateToken(_dbResult.username);
        tokens.refreshToken = token;

        return tokens;
    }
}