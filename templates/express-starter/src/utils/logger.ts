/*
 * @Author: joe
 * @Date: 2024-03-13 10:05:32
 * @LastEditTime: 2024-04-10 18:09:12
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\utils\logger.ts
 */
import { existsSync, mkdirSync } from 'fs';
import { join } from 'path';
import winston from 'winston';
import winstonDaily from 'winston-daily-rotate-file';
import { LOG_DIR, LOG_LEVEL } from '@config';

// log folder
const logFolder: string = join(__dirname, LOG_DIR);
if (!existsSync(logFolder)) {
    mkdirSync(logFolder);
}

const logFormat = winston.format.printf(({ timestamp, level, message }) => `${timestamp} ${level}: ${message}`);

const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
        }),
        logFormat,
    ),
    transports: [
        new winstonDaily({
            level: LOG_LEVEL,
            datePattern: 'YYYY-MM-DD',
            dirname: logFolder,
            filename: `debug-%DATE%.log`,
            maxFiles: 30, // 30 Days retention period
            json: false,
            zippedArchive: true,
        }),
        new winstonDaily({
            level: 'error',
            datePattern: 'YYYY-MM-DD',
            dirname: logFolder,
            filename: `error-%DATE%.log`,
            maxFiles: 30, // 30 Days retention period
            json: false,
            zippedArchive: true,
        }),
    ],
});

logger.add(
    new winston.transports.Console({
        format: winston.format.combine(winston.format.splat(), winston.format.colorize()),
    }),
);

const stream = {
    write: (message: string) => {
        logger.info(message.substring(0, message.lastIndexOf('\n')));
    },
};

export { logger, stream };