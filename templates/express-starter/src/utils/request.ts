/*
 * @Author: joe
 * @Date: 2024-04-25 16:12:54
 * @LastEditTime: 2024-04-26 17:04:46
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\utils\request.ts
 */
import Axios, { Method, ResponseType, AxiosResponse, InternalAxiosRequestConfig } from 'axios';
import { HTTP_TIMEOUT } from '@/config/index';

interface IAxiosData {
    url: string
    method: Method
    headers?: any
    json: boolean
    contentType?: string
    data?: any
    params?: any
    timeout?: number
    responseType?: ResponseType
}

const axios = Axios.create({
    timeout: parseInt(HTTP_TIMEOUT as string, 10),
})

axios.defaults.withCredentials = true

axios.defaults.headers.post['Content-Type'] = 'application/json'

axios.interceptors.request.use(
    (config: InternalAxiosRequestConfig) => {
        return config;
    },
    (err) => Promise.reject(err)
)

// Axios interceptors
axios.interceptors.response.use(
    (res: AxiosResponse) => res,
    (err) => {
        return Promise.reject(err)
    }
)

export default function request(arr: IAxiosData) {
    return new Promise<any>((resolve, reject) => {
        axios({
            url: arr.url,
            method: arr.method || 'POST',
            headers: {
                'content-type': arr.contentType
                    ? arr.contentType
                    : arr.json
                        ? 'application/json; charset=UTF-8'
                        : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            params: arr.params || '',
            data: arr.data || '',
            responseType: arr.responseType || 'json'
        })
            .then((response: AxiosResponse<any>) => {
                const responseStatus = `${response.status}`
                if (responseStatus.charAt(0) === '2') {

                    resolve(response.data)
                } else {
                    reject(response.data)
                }
            })
            .catch((err) => {
                reject(err)
            })
    })
}