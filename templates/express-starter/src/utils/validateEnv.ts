/*
 * @Author: joe
 * @Date: 2024-04-12 09:40:40
 * @LastEditTime: 2024-04-12 09:41:16
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\utils\validateEnv.ts
 */
import { cleanEnv, port, str } from 'envalid';

export const ValidateEnv = () => {
  cleanEnv(process.env, {
    NODE_ENV: str(),
    PORT: port(),
  });
};