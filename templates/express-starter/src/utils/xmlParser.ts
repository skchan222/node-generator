/*
 * @Author: joe
 * @Date: 2024-04-29 10:56:40
 * @LastEditTime: 2024-04-29 11:05:23
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\utils\xmlParser.ts
 */
import { XMLValidator, XMLParser } from "fast-xml-parser";

const parserOptions = {
    attrPrefix: "@_",
    textNodeName: "#text",
    ignoreNonTextNodeAttr: true,
    ignoreTextNodeAttr: true,
    ignoreNameSpace: true,
    ignoreRootElement: false,
    textNodeConversion: true,
    textAttrConversion: false
};

export default function(xmlData: string) {
    if (XMLValidator.validate(xmlData)) {
        const parser = new XMLParser(parserOptions);
        return parser.parse(xmlData);
    }
    return null;
} 