/*
 * @Author: joe
 * @Date: 2024-03-13 16:27:20
 * @LastEditTime: 2024-03-13 17:46:47
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\middlewares\validate.middleware.ts
 */
import { plainToInstance } from 'class-transformer';
import { validateOrReject, ValidationError } from 'class-validator';
import { NextFunction, Request, Response } from 'express';
import { HttpException } from '@exceptions/http.exceptions';

export const validator = (type: any, validatorOptions = { skipMissingProperties: false, whitelist: false, forbidNonWhitelisted: false }) => {
    return (request: Request, response: Response, next: NextFunction) => {
        const inputObj = plainToInstance(type, request.body);
        validateOrReject(inputObj, validatorOptions)
            .then(() => {
                request.body = inputObj;
                next();
            })
            .catch((error: ValidationError[]) => {
                const message = error.map((err: ValidationError) => Object.values(err.constraints)).join(', ');
                next(new HttpException(400, message));
            });
    }
}

export const objIdValidator = async (req: Request, res: Response, next: NextFunction) => {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        next();
    } else {
        next(new HttpException(400, "Invalid ID"));
    }
}