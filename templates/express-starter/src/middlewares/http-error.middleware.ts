/*
 * @Author: joe
 * @Date: 2024-03-12 17:45:18
 * @LastEditTime: 2024-04-10 10:00:52
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\middlewares\http-error.middleware.ts
 */
import { Middleware, ExpressErrorMiddlewareInterface, HttpError } from 'routing-controllers';

@Middleware({ type: 'after' })
export class HttpErrorHandler implements ExpressErrorMiddlewareInterface {
    error(error: any, request: any, response: any, next: (err: any) => any) {

        const status: number = error.status || 500;
        const errMessage: string = error.message || 'System error';
        const retMessage = { status: status, message: errMessage }
        if (error instanceof HttpError) {
            response.status(status).json(retMessage);
        } else {
            response.status(500).json(retMessage);
        }

        next(error);
    }
}