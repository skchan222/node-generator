/*
 * @Author: joe
 * @Date: 2024-03-15 09:16:34
 * @LastEditTime: 2024-04-09 09:58:23
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\middlewares\auth.middleware.ts
 */
import { NextFunction, Request, Response } from 'express';
import { verify, TokenExpiredError } from 'jsonwebtoken';
import { logger } from '@/utils/logger';
import { SECRET_KEY } from '@config';
import { HttpException } from '@/exceptions/http.exceptions';
import { UserModel } from '@/dao/user.dao';
import { User } from '@/interfaces/user.interface';
import { TokenData, UserInRequest } from '@/interfaces/auth.interface';


const getToken = (req) => {
    if ( req.headers.authorization && req.headers.authorization.split(' ')[0] == 'Bearer'){
        return req.headers.authorization.split(' ')[1];
    }
    return null;
}

export const AuthMiddleware = async (req: UserInRequest, res: Response, next: NextFunction) => {
    try {
        const token = getToken(req);
        if (token) {
            const { username } = (await verify(token, SECRET_KEY)) as TokenData;
            const userInDB: User = await UserModel.findOne({ username: username })

            if (userInDB) {
                req.user = userInDB;
                next();
            } else {
                next(new HttpException(401, 'Invalid credentials'));
            }
        } else {
            next(new HttpException(401, 'Invalid credentials'));
        }
    } catch (err) {
        logger.error( "AuthMiddleware " + err.message);
        if (err instanceof TokenExpiredError) {
            next(new HttpException(500, 'JWT token expired, please refresh your token'));
        }
        next(new HttpException(500, err.message || 'Sysstem Error'));
    }
}