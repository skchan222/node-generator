/*
 * @Author: joe
 * @Date: 2024-04-09 11:54:50
 * @LastEditTime: 2024-04-09 11:58:09
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\middlewares\error.middleware.ts
 */
import { NextFunction, Request, Response } from 'express';
import { HttpException } from '@/exceptions/http.exceptions';
import { logger } from '@utils/logger';

export const ErrorHandler = (error: HttpException, req: Request, res: Response, next: NextFunction) => {
    try {
        const code: number = error.status || 500;
        const message: string = error.message || 'System Error';

        logger.error(`[${req.method}] ${req.path} - StatusCode:: ${code}, Message:: ${message}`);
        res.status(code).json({ message });
    } catch (err) {
        next(error);
    }
} 