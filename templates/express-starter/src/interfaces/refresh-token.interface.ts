/*
 * @Author: joe
 * @Date: 2024-04-08 12:01:53
 * @LastEditTime: 2024-04-08 15:36:56
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\interfaces\refreshToken.interface.ts
 */
export interface RefreshToken {
    userID: string;
    username: string;
    token: string;
    expires: Date;
}