/*
 * @Author: joe
 * @Date: 2024-04-29 11:14:23
 * @LastEditTime: 2024-04-29 12:06:29
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\interfaces\global-response.interface.ts
 */
export interface globalResponse {
    result: string;
    data?: any;
    error?: unknown;
}