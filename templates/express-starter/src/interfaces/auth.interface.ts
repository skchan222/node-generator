import { Request } from 'express';
import { User } from '@/interfaces/user.interface';

export interface UserToken {
    token: string;
    refreshToken?: string;
}

export interface TokenData {
    username: string;
}

export interface UserInRequest extends Request {
    user: User;
}