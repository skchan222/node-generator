/*
 * @Author: joe
 * @Date: 2024-03-14 14:31:53
 * @LastEditTime: 2024-03-14 14:37:25
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\interfaces\user.interface.ts
 */
export interface User {
    email: string;
    username: string;
    password: string;
}