/*
 * @Author: joe
 * @Date: 2024-04-26 12:02:37
 * @LastEditTime: 2024-04-29 14:43:16
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\controllers\request.controller.ts
 */
import { NextFunction, Request, Response } from 'express';
import { Controller, Param, Body, Get, Post, Patch, Put, Delete, HttpCode, UseBefore, Req, Res } from 'routing-controllers';
import { Container } from 'typedi';
import { OpenAPI } from 'routing-controllers-openapi';
import { RequestService } from '@/services/request.service';
import { globalResponse } from '@/interfaces/global-response.interface';
import xmlParser from '@/utils/xmlParser';

@Controller()
export class RequestController {
    public requestService = Container.get(RequestService);

    @Post('/api/request/testing')
    @OpenAPI({ summary: 'HTTP request testing' })
    async getDetails(@Req() request: Request, @Res() response: Response): Promise<globalResponse> {
        try {
            const resp: any = await this.requestService.getDetails();
            return { result: 'succ', data: resp.data }
        } catch (err) {
            return { result: 'succ', data: err.response.data }
        }
    }

    @Post('/api/request/xml')
    @OpenAPI({ summary: 'XML parser testing' })
    async getXMLFormat(@Req() request: Request, @Res() response: Response): Promise<globalResponse> {
        const xmlData = `<root>
                        <int>   1234    </int>
                        <str>4567</str>
                        <int>str 6789</int>
                        <bool>true  </bool>
                        </root>`
        let result = xmlParser(xmlData);
        if (result === null)
            return { result: 'fail', error: "XML parser error" };
        return { result: 'succ', data: result };
    }
}