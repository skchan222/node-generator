/*
 * @Author: joe
 * @Date: 2024-03-12 17:55:06
 * @LastEditTime: 2024-04-12 11:31:42
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\controllers\user.controller.ts
 */
import { NextFunction, Request, Response } from 'express';
import { Controller, Param, Body, Get, Post, Patch, Put, Delete, HttpCode, UseBefore, Req, Res } from 'routing-controllers';
import { Container } from 'typedi';
import { OpenAPI } from 'routing-controllers-openapi';
import { User } from '@/interfaces/user.interface';
import { UserService } from '@/services/user.service';
import { objIdValidator } from '@/middlewares/validate.middleware';
import { AuthMiddleware } from '@/middlewares/auth.middleware';

@Controller()
export class UserController {
    public userService = Container.get(UserService);

    @Get('/api/users')
    @UseBefore(AuthMiddleware)
    @OpenAPI({ summary: 'Get a list of users' , security: [{ bearerAuth: [] }] })
    async getUsers(@Req() request: Request, @Res() response: Response) {
        try {
            const allUsers: User[] = await this.userService.getAllUsers();
            return response.status(200).json({ data: allUsers, message: 'Success' });
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Get('/api/user/:id')
    @UseBefore(objIdValidator)
    @OpenAPI({ summary: 'Get a specific user' })
    async getUser(@Param("id") id: string, @Res() response: Response) {
        return response.status(200).json({ data: await this.userService.getUserById(id), message: 'Success' });
    }

    @Patch('/api/user/:id')
    @OpenAPI({ summary: 'update the user information' })
    async updateUser(@Param("id") id: string, @Body() user: User, @Res() response: Response) {
        return response.status(200).json({ data: await this.userService.updateUser(id, user), message: 'Success' });
    }

    @Delete('/api/user/:id')
    @OpenAPI({ summary: 'delete the user' })
    async deleteUser(@Param("id") id: string, @Res() response: Response) {
        return response.status(200).json({ data: await this.userService.deleteUser(id), message: 'Success' });
    }
}