/*
 * @Author: joe
 * @Date: 2024-04-09 10:19:39
 * @LastEditTime: 2024-04-10 10:00:33
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\controllers\not-found.controller.ts
 */
import { Controller, Req, Body, Get, Post, UseBefore, HttpCode, Res } from 'routing-controllers';
import { HttpException } from '@/exceptions/http.exceptions';

@Controller()
export class NotFoundController {

    @Get('/:rest*')
    async getNotFound() {
        throw new HttpException(404, 'Not Found');
    }

    @Post('/:rest*')
    async postNotFound() {
        throw new HttpException(404, 'Not Found');
    }

}