/*
 * @Author: joe
 * @Date: 2024-03-15 09:15:30
 * @LastEditTime: 2024-04-09 18:00:19
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\controllers\auth.controller.ts
 */
import { Controller, Req, Body, Post, UseBefore, HttpCode, Res } from 'routing-controllers';
import { Container } from 'typedi';
import { AuthService } from '@/services/auth.service';
import { NewUserDto, loginDto } from '@/dto/user.dto';
import { validator } from '@/middlewares/validate.middleware';
import { HttpException } from '@/exceptions/http.exceptions';

@Controller()
export class AuthController {
    public authService = Container.get(AuthService);

    @Post('/signup')
    @UseBefore(validator(NewUserDto))
    async signup(@Body() userData: NewUserDto) {
        let newUser = await this.authService.signup(userData);
        delete newUser.password;
        return { data: newUser, message: 'Success' };
    }

    @Post('/login')
    @UseBefore(validator(loginDto))
    async login(@Body() loginData: loginDto) {
        const loginResult = await this.authService.login(loginData);
        return { data: loginResult, message: 'Success' };
    }

    @Post('/refreshToken')
    async refreshToken(@Req() req) {
        if ( !req.headers.refreshtoken || req.headers.refreshtoken === ''){
            throw new HttpException(401, 'Token mising');
        }
        // TODO: change the refresh token from header to request body
        const token = req.headers.refreshtoken;
        return this.authService.refreshToken(token);
    }
}