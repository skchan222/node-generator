/*
 * @Author: joe
 * @Date: 2024-03-13 10:05:45
 * @LastEditTime: 2024-04-25 16:15:14
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\config\index.ts
 */
import { config } from 'dotenv';

config({ path: `.env.${process.env.NODE_ENV || 'development'}` });

export const CREDENTIALS = process.env.CREDENTIALS === 'true';
export const {
    NODE_ENV,
    PORT, SECRET_KEY,
    LOG_LEVEL,
    LOG_FORMAT,
    LOG_DIR,
    ORIGIN,
    CONNECTION_STR,
    EXPIRE_INTERVAL,
    REFRESH_INTERVAL,
    SWAGGER,
    RATE_CONTROL,
    RATE_LIMIT_INTERVAL,
    RATE_LIMIT_REQUEST,
    HTTP_TIMEOUT } = process.env;