/*
 * @Author: joe
 * @Date: 2024-04-29 16:59:08
 * @LastEditTime: 2024-04-29 16:59:19
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\test\request.test.ts
 */
import request from 'supertest';
import mongoose from 'mongoose';
import { App } from '@/app';
import { RequestController } from '@/controllers/request.controller';

afterAll(async () => {
    await new Promise<void>(resolve => setTimeout(() => resolve(), 1000));
    await mongoose.disconnect();
})

describe('Testing Request Controller', () => {
    describe('[POST Test] /api/request/xml', () => {
        it('should be able to call without any error', () => {
            const app = new App([RequestController]);
                return request(app.getApp())
                    .post('/api/request/xml')
                    .expect(200);
        })
    })
})