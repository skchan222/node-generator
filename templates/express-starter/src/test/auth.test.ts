/*
 * @Author: joe
 * @Date: 2024-04-10 10:08:28
 * @LastEditTime: 2024-04-11 15:17:03
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\test\auth.test.ts
 */
import request from 'supertest';
import mongoose from 'mongoose';
import { App } from '@/app';
import { AuthController } from '@/controllers/auth.controller';
import { NewUserDto, loginDto } from '@/dto/user.dto';

afterAll(async () => {
    await new Promise<void>(resolve => setTimeout(() => resolve(), 1000));
    await mongoose.disconnect();
})

describe('Testing Auth Controller', () => {
    describe('[Post Test] /signup', () => {
        it('should be able to reject the signup request with 400 error', () => {
            const newUser: NewUserDto = {
                username: 'tes',
                password: '1234567822222',
                email: 'testing22222222@test.com'
            };
            const app = new App([AuthController]);
            return request(app.getApp())
                .post('/signup')
                .send(newUser)
                .expect(400)
                .then(response => {
                    expect(response.body.message).toEqual('username is too short')
                });
        });
    });

    describe('[Post Test] /login', () => {
        it('should be able to reject the login with testing user account', () => {
            const login: loginDto = {
                username: 'tes',
                password: '1234567822222',
            };
            const app = new App([AuthController]);
            return request(app.getApp())
                .post('/login')
                .send(login)
                .expect(401)
                .then(response => {
                    expect(response.body.message).toEqual('invalid username or password')
                });
        });
    });
});