/*
 * @Author: joe
 * @Date: 2024-04-11 15:22:26
 * @LastEditTime: 2024-04-29 17:28:49
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\test\user.test.ts
 */
import request from 'supertest';
import mongoose from 'mongoose';
import { App } from '@/app';
import { UserController } from '@/controllers/user.controller';

afterAll(async () => {
    await new Promise<void>(resolve => setTimeout(() => resolve(), 1000));
    await mongoose.disconnect();
})

describe('Testing User Controller', () => {
    describe('[GET Test] /api/users', () => {
        it('should be reject by invalid token', () => {
            const app = new App([UserController]);
            return request(app.getApp())
                .get('/api/users')
                .set('Authorization', 'abc123')
                .expect(401)
                .then(response => {
                    expect(response.body.message).toEqual('Invalid credentials')
                });
        })
    });
});