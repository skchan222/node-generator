/*
 * @Author: joe
 * @Date: 2024-03-13 09:17:08
 * @LastEditTime: 2024-04-26 14:34:45
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\express-starter\src\server.ts
 */
import { App } from '@/app';

import { UserController } from '@/controllers/user.controller';
import { AuthController } from '@/controllers/auth.controller';
import { RequestController } from '@/controllers/request.controller';
import { NotFoundController } from '@/controllers/not-found.controller';

const app = new App([UserController, AuthController, RequestController, NotFoundController]);
app.listen();