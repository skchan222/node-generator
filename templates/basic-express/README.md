# basic-express

Start your development with the help of below usage

## Usage

local devlopment with nodemon
```bash
npm run dev
```

build the production server
```bash
npm run build
```

start the server with production mode
```bash
npm start
```

