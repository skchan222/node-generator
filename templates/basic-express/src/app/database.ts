/*
 * @Author: joe
 * @Date: 2024-03-05 09:28:40
 * @LastEditTime: 2024-03-05 10:12:58
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\app\database.ts
 */
import mongoose from 'mongoose';
import config from '../config';

function handleError(err: any) {
    console.log("DB Error: " + err);
    process.exit(1);
}

export default async function connect() {
    await mongoose.connect(config.connectionStr)
                .catch(error => handleError(error));
}
