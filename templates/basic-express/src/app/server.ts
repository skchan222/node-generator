/*
 * @Author: joe
 * @Date: 2024-02-14 11:50:04
 * @LastEditTime: 2024-03-12 12:15:23
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\app\server.ts
 */
import { useExpressServer, useContainer, Action } from 'routing-controllers';
import express, { NextFunction, Request, Response } from 'express';
import cors, { CorsOptions } from 'cors';
import morgan from 'morgan';
import Container, { Service } from 'typedi';
import { GenericResponse } from '@/interface/GeneralResp';
import { Secret, decode, verify } from 'jsonwebtoken';
import UserController from '../controllers/UserController';
import UserAuthController from '../controllers/UserAuthController';
import logger from '../utils/logger';
import connect from './database';
import User from '../model/User';
import config from '../config';
import {ErrorMiddleware} from '../middleware/ErrorHandler';
import {FinalMiddleware} from '../middleware/FinalMiddleware';

export default class Server {
  public app: express.Application;

  constructor() {
    // useContainer(Container);
    this.app = express();
    this.setup();
    this.setRoute();
    this.setControllers();
  }

  public setup() {
    this.app.disable('x-powered-by');
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(ErrorMiddleware);
    this.app.use(
      morgan(function (tokens: any, req: any, res: any) {
        return [
          `${new Date().toISOString()}`,
          tokens.method(req, res),
          tokens.url(req, res),
          tokens.status(req, res),
          tokens.res(req, res, 'content-length'), '-',
          tokens['response-time'](req, res), 'ms'
        ].join(' ')
      })
    );
  }

  public setRoute() {
    console.log('setting route');
    this.app.get('*', (req: any, res: GenericResponse, next) => {
      if (req.url.indexOf('/api') !== -1) {
        next();
      } else {
        res.json({
            resultCode: 200,
            message: 'It works!!',
        });
      }
    });
  }

  public run(port: number) {
    this.connectDB();
    this.app.listen(port, () => {
      console.log(`Express API listening on port ${port}`);
      logger.info(`Express API listening on port ${port}`);
    });
  }

  public setControllers() {
    console.log('setting controller');
    useExpressServer(this.app, {
      routePrefix: 'api',
      controllers: [ UserController, UserAuthController ],
      authorizationChecker: async (action: Action, roles: string[]) => {
        let token = ''
        if ( action.request.headers.authorization && action.request.headers.authorization.split(' ')[0] == 'Bearer'){
          token = action.request.headers.authorization.split(' ')[1];
        }
        if ( token === '' )
          throw new Error('empty token')
        var decoded: any = verify(token, config.jwtSecret);
        const u = await User.countDocuments({username: decoded.name});
        console.log(u)
        if ( u == 1 )
          return true;
        return false;
      },
      middlewares: [FinalMiddleware],
      defaultErrorHandler: false,
    });
  }

  private getToken(action: Action) {
    if ( action.request.headers.authorization && action.request.headers.authorization.split(' ')[0] == 'Bearer'){
      return action.request.headers.authorization.split(' ')[1];
    }
    return null;
  }

  public connectDB() {
    console.log('connecting to mongo database');
    logger.info('connecting to mongo database');
    connect();
  }

}