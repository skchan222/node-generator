/*
 * @Author: joe
 * @Date: 2024-03-06 09:41:47
 * @LastEditTime: 2024-03-06 10:19:23
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\utils\hashing.ts
 */
import bcrypt from "bcrypt";

export default async function hashing(data: string) {
    const saltRounds = 10;
    const hash: string = await bcrypt.hash(data, saltRounds);
    return hash;
}