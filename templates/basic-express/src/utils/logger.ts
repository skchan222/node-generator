/*
 * @Author: joe
 * @Date: 2024-02-16 10:49:53
 * @LastEditTime: 2024-02-27 16:35:35
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\utils\logger.ts
 */
import winston from 'winston';
import config from '../config';

const logger = winston.createLogger({
    level: config.logLevel,
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
      new winston.transports.File({ filename: 'error.log', level: 'error' }),
      new winston.transports.File({ filename: 'app.log' }),
    ],
});

export default logger