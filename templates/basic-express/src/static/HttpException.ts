/*
 * @Author: joe
 * @Date: 2024-03-11 15:49:48
 * @LastEditTime: 2024-03-12 09:46:30
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\static\HttpException.ts
 */
import { HttpError } from 'routing-controllers';

export class HttpException extends HttpError {
  public status: number;
  public message: string;

  constructor(status: number, message: string) {
    super(status, message);
    this.status = status;
    this.message = message;
  }
}