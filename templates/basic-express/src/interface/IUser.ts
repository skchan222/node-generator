/*
 * @Author: joe
 * @Date: 2024-02-27 15:01:13
 * @LastEditTime: 2024-03-05 11:02:08
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\interface\User.ts
 */
export interface IUser {
    email: string;
    username: string;
    password: string;
}

export interface Ilogin {
    username: string;
    password: string;
}