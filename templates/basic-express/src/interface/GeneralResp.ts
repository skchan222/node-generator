import express from 'express';

interface MessageResponse {
  resultCode: number,
  message: string;
}

type Send<T = Response> = (body: MessageResponse) => T;

interface GenericResponse extends express.Response {
  json: Send<this>;
}

export { GenericResponse, MessageResponse };