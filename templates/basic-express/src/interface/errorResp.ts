import { MessageResponse } from './GeneralResp';

export default interface ErrorResponse extends MessageResponse {
  error?: string;
}