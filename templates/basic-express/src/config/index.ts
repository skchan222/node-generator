/*
 * @Author: joe
 * @Date: 2024-02-15 15:09:59
 * @LastEditTime: 2024-03-05 09:32:23
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\config\index.ts
 */
import dotenv from 'dotenv';
import * as jwt from 'jsonwebtoken';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const isEnvFileExist = dotenv.config();
if (isEnvFileExist.error) {
    console.error('.env file not found!')
}

const config = {
    serverPort: parseInt(process.env.PORT as string, 10) || 3000,

    jwtSecret: process.env.JWT_SECRET || 'mysecret',
    jwtAlgorithm: process.env.JWT_ALGORITHM?.split(',') as jwt.Algorithm[] || ['RS512'] as jwt.Algorithm[],
    jwtPeriod: parseInt(process.env.JWT_PERIOD as string, 10) || 10,

    logLevel: process.env.LOG_LEVEL || 'info',

    connectionStr: process.env.CONNECTION_STR as string,
}

export default config