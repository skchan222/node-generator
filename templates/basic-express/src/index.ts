/*
 * @Author: joe
 * @Date: 2024-02-14 10:43:05
 * @LastEditTime: 2024-02-16 10:41:41
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\index.ts
 */
import 'reflect-metadata';
import Server from './app/server';
import config from './config';

const port: number = config.serverPort;
const app = new Server();
app.run(port);
