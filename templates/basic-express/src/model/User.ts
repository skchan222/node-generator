/*
 * @Author: joe
 * @Date: 2024-03-05 11:00:46
 * @LastEditTime: 2024-03-07 12:08:04
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\model\User.ts
 */
import mongoose from 'mongoose';
import { IUser } from '../interface/IUser';

const AutoIncrement = require('mongoose-sequence')(mongoose);

const User = new mongoose.Schema(
    {
        username: {
            type: String,
            required: [true, 'Please enter a anme'],
            index: true,
        },
        password: {
            type: String,
        },
        email: {
            type: String,
            index: true,
        },
    },
    { timestamps: true },
)
User.plugin(AutoIncrement, { inc_field: 'id' })

export default mongoose.model<IUser & mongoose.Document>('User', User);