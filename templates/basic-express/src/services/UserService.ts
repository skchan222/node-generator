/*
 * @Author: joe
 * @Date: 2024-02-15 09:32:21
 * @LastEditTime: 2024-03-07 12:06:49
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\services\UserService.ts
 */
import { Service } from 'typedi';
import { IUser, Ilogin } from '../interface/IUser';
import User from '../model/User';
import hashing from '../utils/hashing';

@Service()
export default class UserService {
    
    constructor() {}

    public async listUsers(): Promise<{result?: any}> {
        try {
            const userList = await User.find().lean().exec();
            return { result: JSON.parse(JSON.stringify(userList)) }     // TODO: enhance this method
        } catch (err) {
            return { result: 'failed to fetch users'}
        }
    }

    public async getUser(id: any): Promise<{result?: any}> {
        try {
            const user = await User.findById(id);
            return { result: JSON.parse(JSON.stringify(user)) }
        } catch (err) {
            return { result: 'failed to fetch user'}
        }
    }

    public async updateUser(id: any, userInfo: IUser): Promise<{result?: any}> {
        const options = { new: true };
        try {
            const result = await User.findByIdAndUpdate(
                id, userInfo, options
            )
            return { result: 'succ' }
        } catch (err) {
            return { result: 'failed to update user'}
        }
    }

    public async deleteUser(id: any): Promise<{result?: any}> {
        try {
            const result = await User.findByIdAndDelete(id)
            return { result: 'succ' }
        } catch (err) {
            return { result: 'failed to delete user'}
        }
    }


    public async userSignUp(user: IUser): Promise<{result: any, errMsg?: string}> {
        let userObj = new User(user);
        const hash_password: string = await hashing(userObj.password);
        userObj.password = hash_password;
        try {
            const newUser = await userObj.save();
        } catch (err: any) {
            return { result: 'Fail', errMsg: err.message}
        }
        return { result: 'Succ'}
    }

}