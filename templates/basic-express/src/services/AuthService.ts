/*
 * @Author: joe
 * @Date: 2024-02-27 11:35:28
 * @LastEditTime: 2024-03-06 15:40:13
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\services\AuthService.ts
 */
import { Service } from 'typedi';
import jwt from 'jsonwebtoken';
import bcrypt from "bcrypt";
import User from '../model/User';
import { Ilogin } from '../interface/IUser';
import logger from '../utils/logger';
import config from '../config/';


@Service()
export default class AuthService {
    
    constructor() {}

    public async login(credential: Ilogin): Promise<{token?: string, msg: string}> {
        const user = await User.findOne({username: credential.username});
        const invalidPasswd = { msg: 'Invalid username or password' }
        if (user === null)
            return invalidPasswd;

        const validity = await bcrypt.compare(credential.password, user.password);
        if (validity) {
            const token = this.generateJWT(credential.username);
            return { token: token, msg: 'succ' }
        } else {
            return invalidPasswd;
        }
    }

    private generateJWT(username: string) {
        const today = new Date();
        const expire = new Date(today);
        expire.setDate(today.getDate() + config.jwtPeriod); //should be configurable

        logger.debug("generating JWT token for user " + username);
        return jwt.sign({
            name: username,
            exp: expire.getTime() / 1000
            },
            config.jwtSecret
        )
    }
}