import { NextFunction, Request, Response } from 'express';
import { HttpException } from '../static/HttpException';

export const ErrorMiddleware = (error: HttpException, req: Request, res: Response, next: NextFunction) => {
  try {
    const status: number = error.status || 500;
    const message: string = error.message || 'Something went wrong';

    const retMessage = { status: status, message: message }

    console.log(`[${req.method}] ${req.path} >> StatusCode:: ${status}, Message:: ${message}`);
    res.status(status).json(retMessage);
  } catch (error) {
    next(error);
  }
};