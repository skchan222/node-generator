/*
 * @Author: joe
 * @Date: 2024-02-14 17:48:50
 * @LastEditTime: 2024-03-07 15:06:21
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\middleware\isLogin.ts
 */
import { expressjwt } from "express-jwt";
import config from '../config'

const getTokenFromRequest = (req: any) => {
    
    if ( req.headers.authorization && req.headers.authorization.split(' ')[0] == 'Bearer'){
        return req.headers.authorization.split(' ')[1];
    }
    return null;
};

const isLogin = expressjwt({
    secret: config.jwtSecret,
    algorithms: config.jwtAlgorithm,
    getToken: getTokenFromRequest,
})

export default isLogin;