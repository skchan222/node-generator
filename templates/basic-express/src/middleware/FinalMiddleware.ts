/*
 * @Author: joe
 * @Date: 2024-03-11 12:15:30
 * @LastEditTime: 2024-03-11 14:23:21
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\middleware\FinalMiddleware.ts
 */
import { Middleware, ExpressMiddlewareInterface } from 'routing-controllers';
import { NextFunction, Request, Response } from 'express';
import { Service} from 'typedi';

@Service()
export class FinalMiddleware implements ExpressMiddlewareInterface {

  public use(req: Request, res: Response, next?: NextFunction): void {
    console.log('FinalMiddleware reached, ending response.');
    if(!res.headersSent) {
      // TODO: match current url against every registered one
      // because finalhandler is reached if no value is returned in the controller.
      // so we need to set 404 only if really there are no path handling this.
      // or we just have to return with null?
      res.status(404);
      res.send({ id: req.path, type: 'route' });
    }
    res.end();
  }

}