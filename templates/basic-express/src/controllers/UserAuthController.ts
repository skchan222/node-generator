/*
 * @Author: joe
 * @Date: 2024-02-16 10:33:52
 * @LastEditTime: 2024-03-06 11:41:55
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\controllers\UserAuthController.ts
 */
import { Request, Response } from 'express';
import { Service, Container, Inject } from 'typedi';
import { Controller, Req, Res, Post, Param, Body } from 'routing-controllers';
import AuthService from '../services/AuthService';
import { Ilogin } from '../interface/IUser';

// @Service()
@Controller()
export default class UserAuthController {

    constructor(
        private authService: AuthService
    ) {}

    @Post('/login')
    public login(@Body() credential: Ilogin, @Res() response: Response) {
        return this.authService.login(credential);
    }

}