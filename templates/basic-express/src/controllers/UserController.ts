/*
 * @Author: joe
 * @Date: 2024-02-14 17:55:37
 * @LastEditTime: 2024-03-12 12:16:13
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\templates\basic-express\src\controllers\UserController.ts
 */
import { Request, Response } from 'express';
import { Controller, Req, Res, Get, Post, Delete, Patch, Param, Body, Authorized, UseAfter } from 'routing-controllers';
import { Service, Container, Inject } from 'typedi';
import UserService from '../services/UserService';
import { IUser } from '../interface/IUser';
import { Secret, decode, verify } from 'jsonwebtoken';
import config from '../config';
import User from '../model/User';
import {FinalMiddleware} from '../middleware/FinalMiddleware';


// @JsonController()  //TODO: need further study
// @Service()
@Controller()
export default class UserController {
  constructor(
    private userService: UserService
  ) {}

  @Get('/user/list')
  public listUsers(@Req() request: Request, @Res() response: Response) {
    return this.userService.listUsers();
  }

  @Get('*')
  public notfound(@Req() request: Request, @Res() response: Response) {
    throw new Error('not found');
  }

  @Authorized()
  @Post('/testing')
  public testing(@Req() request: Request, @Res() response: Response) {
    let token = ''
    if ( request.headers.authorization && request.headers.authorization.split(' ')[0] == 'Bearer'){
      token = request.headers.authorization.split(' ')[1];
    }
    if ( token === '' )
      throw new Error('empty token')
    verify(token, config.jwtSecret, async function(err, decoded: any) {
      if ( decoded !== undefined ) {
        const u = await User.countDocuments({username: decoded.name});
        if ( u == 1 )
          console.log('password correct')
      } else {
        console.log('invalid user')
      }
    });
    return this.userService.listUsers();
  }

  @Get('/user/:id')
  public getUser(@Param("id") id: any, @Res() response: Response) {
    return this.userService.getUser(id);
  }

  @Patch('/user/:id')
  public updateUser(@Param("id") id: any, @Body() user: IUser, @Res() response: Response) {
    return this.userService.updateUser(id, user);
  }

  @Delete('/user/:id')
  public deleteUser(@Param("id") id: any, @Res() response: Response) {
    return this.userService.deleteUser(id);
  }

  @Post('/user/signup')
  public signup(@Body() user: IUser, @Res() response: Response) {
    return this.userService.userSignUp(user);
  }

}