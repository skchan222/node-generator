/*
 * @Author: joe
 * @Date: 2024-02-14 10:43:05
 * @LastEditTime: 2024-03-14 09:21:23
 * @LastEditors: joe
 * @Description: 
 * @FilePath: \node-generator\src\index.ts
 */
import prompts, { PromptObject } from "prompts";
import fs from 'fs'
import path from "path";
import shell from 'shelljs'

const questions: PromptObject[] = [
    {
        type: 'text',
        name: 'projectName',
        message: 'What is the name of your new project .',
        validate: name => name.trim().length < 1 || typeof name !== 'string' ?
            'Project name is required' : true

    },
    {
        type: 'select',
        name: 'templateName',
        message: 'Which template do you want to generate',
        validate: name => name.trim().length < 1 ?
            'Template name is required' : true,
        choices: [
            {
                title: 'Basic Express API',
                value: 'basic-express'
            },
            {
                title: 'Express Starter',
                value: 'express-starter'
            },
            {
                title: 'dummy',
                value: 'dummy'
            },
        ]
    }
];

const replicateTemplates = (
    templatePath: string, projectPath: string
) => {
    //Get template file names
    let templateContentNames = fs.readdirSync(templatePath);
    //filter out skip list
    const filesToBeSkipped = ['node_modules', 'build', 'dist']

    templateContentNames = templateContentNames.filter(
        (name: string) => !filesToBeSkipped.includes(name)
    )
    if (!fs.existsSync(projectPath)) {
        fs.mkdirSync(projectPath)
    } else {
        console.error(
            'Directory already exists. Choose another name'
        )
        return
    }

    templateContentNames.forEach((name: any) => {
        const originPath = path.join(templatePath, name)
        const destinationPath = path.join(projectPath, name)
        const stats = fs.statSync(originPath)
        if (stats.isFile()) {
            const content = fs.readFileSync(originPath, 'utf8')
            fs.writeFileSync(destinationPath, content)

        } else if (stats.isDirectory()) {
            replicateTemplates(originPath, destinationPath)
        }
    })

    console.log('post generateing job')
    if (templateContentNames.includes('package.json')) {
        //change directory into project path
        shell.cd(projectPath)

        shell.exec('npm install')

        shell.cd('../')

        // move the new generated project to the parent directory
        shell.exec('mv ' + projectPath + ' ../')
    }

}

prompts(questions)
    .then((answers) => {
        const projectName = answers.projectName
        const templateName = answers.templateName

        const projectPath = path.join(process.cwd(), projectName)

        const templatePath = path.join(process.cwd(), 'templates', templateName)

        replicateTemplates(templatePath, projectPath)
    }
    )
    .catch(error => console.error(error.message))